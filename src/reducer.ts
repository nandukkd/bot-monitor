export type joinApisType = {
	pending: number,
	success: number,
	failed: number,
}

export type processType = {
	bots: number,
	url: string,
	joinApis: joinApisType,
	sockets: number,
	connected: boolean,
}

export type questionType = {
	asked: number,
	answered: Record<string, number>,
	pending: number,
	failed: number
};

export type state = {
	running: boolean,
	sockets: number,
	bots: number,
	joinApis: joinApisType,
	processes: Record<string, processType>,
	questions: Record<string, questionType>,
}
export const defaultState = {
	running: false,
	sockets: 0,
	bots: 0,
	joinApis: {pending: 0, success: 0, failed: 0},
	processes: {},
	questions: {},
}

function clear (state: state): state {
	let pr = {...state.processes};
	for(let i in pr) {
		pr[i] = {...pr[i], sockets: 0, bots: 0, joinApis: {pending: 0, success: 0, failed: 0}};
	}
	return {
		running: false,
		bots: 0, sockets: 0, joinApis: {pending: 0, success: 0, failed: 0},
		processes: pr,
		questions: {},
	}
}

export type action = {event: 'game_start'}
	| {event: 'game_end'}
	| {event: 'game_clear'}
	| {event: 'process', processId: string, url: string}
	| {event: 'connected', processId: string}
	| {event: 'disconnected', processId: string}
	| {event: 'starting', processId: string}
	| {event: 'api_called', processId: string}
	| {event: 'api_responded', processId: string}
	| {event: 'api_error', processId: string}
	| {event: 'socket_error', processId: string}
	| {event: 'socket_disconnected', processId: string}
	| {event: 'socket_connected', processId: string}
	| {event: 'player_screen_question', payload: {question: {id: string, get_answers_by_question_id: {id: string}[]}}, processId: string}
	| {event: 'player_answer_pending', payload: {questionId: string}, processId: string}
	| {event: 'player_answer_success', payload: {answerId: string, questionId: string}, processId: string}
	| {event: 'player_question_failed', payload: {questionId: string}, processId: string}

export default function reducer (state: state, action: action): state {
	switch(action.event) {
		case 'game_start':
			return {...clear(state), running: true};
		case 'game_clear':
			return clear(state);
		case 'game_end':
			return { ...state, running: false };
		case 'process':
			return { ...state, processes: {
				...state.processes,
				[action.processId]: state.processes[action.processId] ? {
					...state.processes[action.processId],
					connected: true
				} : {
					sockets: 0,
					connected: true,
					joinApis: { success: 0, pending: 0, failed: 0 },
					bots: 0,
					url: action.url,
				},
			}};
		case 'connected':
			return { ...state, processes: {
				...state.processes,
				[action.processId]: {
					...state.processes[action.processId],
					connected: true,
				},
			}};
		case 'disconnected':
			return { ...state, processes: {
				...state.processes,
				[action.processId]: {
					...state.processes[action.processId],
					connected: false,
				},
			}};
		case 'starting':
			return { ...state, bots: state.bots+1, processes: {
				...state.processes,
				[action.processId]: {
					...state.processes[action.processId],
					bots: state.processes[action.processId].bots+1,
				},
			}};
		case 'api_called':
			return {...state, joinApis: {...state.joinApis, pending: state.joinApis.pending+1}, processes: {
				...state.processes,
				[action.processId]: {
					...state.processes[action.processId],
					joinApis: {
						...state.processes[action.processId].joinApis,
						pending: state.processes[action.processId].joinApis.pending+1,
					},
				},
			}};
		case 'api_responded':
			return {...state, joinApis: {
				...state.joinApis,
				pending: state.joinApis.pending-1,
				success: state.joinApis.success+1,
			}, processes: {
				...state.processes,
				[action.processId]: {
					...state.processes[action.processId],
					joinApis: {
						...state.processes[action.processId].joinApis,
						pending: state.processes[action.processId].joinApis.pending-1,
						success: state.processes[action.processId].joinApis.success+1,
					},
				},
			}};
		case 'api_error':
			return {...state, joinApis: {
				...state.joinApis,
				pending: state.joinApis.pending-1,
				failed: state.joinApis.failed+1,
			}, processes: {
				...state.processes,
				[action.processId]: {
					...state.processes[action.processId],
					joinApis: {
						...state.processes[action.processId].joinApis,
						pending: state.processes[action.processId].joinApis.pending-1,
						failed: state.processes[action.processId].joinApis.failed+1,
					},
				},
			}};

		case 'socket_error':
			return state;
		case 'socket_disconnected':
			return {...state, sockets: state.sockets-1, processes: {
				...state.processes,
				[action.processId]: {
					...state.processes[action.processId],
					sockets: state.processes[action.processId].sockets-1,
				},
			}};
		case 'socket_connected':
			return {...state, sockets: state.sockets+1, processes: {
				...state.processes,
				[action.processId]: {
					...state.processes[action.processId],
					sockets: state.processes[action.processId].sockets+1,
				},
			}};
		case 'player_screen_question':
			return {...state, questions: {
				...state.questions,
				[action.payload.question.id]: !state.questions[action.payload.question.id] ? {
					asked: 1, failed: 0, pending: 0,
					answered: action.payload.question.get_answers_by_question_id.reduce((a,i) => ({...a, [i.id]: 0}), {}),
				} : {
					...state.questions[action.payload.question.id],
					asked: state.questions[action.payload.question.id].asked+1
				},
			}}
		case 'player_answer_pending':
			return { ...state, questions: {
				...state.questions,
				[action.payload.questionId]: {
					...state.questions[action.payload.questionId],
					pending: state.questions[action.payload.questionId].pending+1,
				},
			}}
		case 'player_answer_success':
			return { ...state, questions: {
				...state.questions,
				[action.payload.questionId]: {
					...state.questions[action.payload.questionId],
					pending: state.questions[action.payload.questionId].pending-1,
					answered: {
						...state.questions[action.payload.questionId].answered,
						[action.payload.answerId]: state.questions[action.payload.questionId].answered[action.payload.answerId]+1,
					},
				},
			}}
		case 'player_question_failed':
			return { ...state, questions: {
				...state.questions,
				[action.payload.questionId]: {
					...state.questions[action.payload.questionId],
					pending: state.questions[action.payload.questionId].pending-1,
					failed: state.questions[action.payload.questionId].failed+1,
				},
			}}
		default:
			return state;
	}
}