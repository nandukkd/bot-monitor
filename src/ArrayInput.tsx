import { ReactElement, useState } from 'react';
import {ReactComponent as CloseIcon} from './assets/close.svg';
import {ReactComponent as AddIcon} from './assets/add.svg';

type InputItemElement<T> = (props: {onChange: (value: T) => void, onCancel: () => void}) => ReactElement;
type ArrayItemElement<T> = (props: {onClose: () => void, value: T}) => ReactElement;


export default function ArrayInput<T>({array, onChange, inputItem: InputItem, arrayItem: ArrayItem}: {
	array: T[],
	onChange: (array: T[]) => void,
	inputItem: InputItemElement<T>,
	arrayItem: ArrayItemElement<T>,
}) {
  const [ newVisible, setNewVisible ] = useState(false);
  const add = (val: T) => {
    onChange([...array, val]);
  }
  const remove = (index: number) => {
    onChange(array.filter((_,i) => i!==index));
  }
  return (
    <div className="array-input-outer">
      {array.map((i,j) => (
        <ArrayItem value={i} onClose={() => remove(j)} key={j} />
      ))}
      {newVisible?(
        <div className="array-input">
          <InputItem onChange={v => {add(v); setNewVisible(false);}} onCancel={() => setNewVisible(false)} />
          <span className='btn' onClick={() => setNewVisible(false)}>
						<CloseIcon className="icon" />
					</span>
        </div>
      ):(
				<span
					className="btn"
					onClick={() => setNewVisible(true)}
				>
					<AddIcon className="icon" />
				</span>
			)}
    </div>
  )
}
