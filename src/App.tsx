import { useRef, useReducer, Reducer, Fragment, useState, useMemo, KeyboardEventHandler, ReactNode } from 'react'
import { io, Socket } from 'socket.io-client'
import ArrayInput from './ArrayInput';
import './App.css'
import reducer, { action, state, processType, questionType, defaultState, joinApisType } from './reducer';
import {ReactComponent as EditIcon} from './assets/edit.svg';
import {ReactComponent as TickIcon} from './assets/tick.svg';
import {ReactComponent as CloseIcon} from './assets/close.svg';
import {ReactComponent as AddIcon} from './assets/add.svg';
import {ReactComponent as RefreshIcon} from './assets/refresh.svg';
import {ReactComponent as StopIcon} from './assets/stop.svg';
import {ReactComponent as PlayIcon} from './assets/play.svg';


export default function App() {
	const [ startModal, setStartModal ] = useState(false);
	const [ addModal, setAddModal ] = useState(false);
	const [ data, dispatch ] = useReducer<Reducer<state, action>>(reducer, defaultState);
	const bots = useRef<Record<string, Socket>>({});
	const connectProcess = (url: string, id?: string) => {
		const processId = id || Object.keys(bots.current).length+'';
		dispatch({event: 'process', processId, url})
		const socket = io(url, {protocols: ['websocket']});//'ws://localhost:4000/');
		bots.current[processId] = socket;
		socket.on('connect', () => {
			dispatch({event: 'connected', processId})
		})
		socket.on('disconnnect', (reason) => {
			dispatch({event: 'disconnected', processId});
		})
		socket.on('connect_error', (reason) => {
			socket.close();
			dispatch({event: 'disconnected', processId});
		})
		socket.on('item', (item) => {
			// dispatch({event: 'starting', processId});
		})
		socket.on('action', (item, eventName, payload) => {
			dispatch({event: eventName, payload, processId});
		})
	}
	const startTest = (gameId: number, teamIds: number[], playersCount: number) => {
		dispatch({event: 'game_start'})
		const botIds = Object.keys(bots.current);
		const perBot = Math.floor(playersCount/botIds.length);
		for(let i=0; i<botIds.length; i++) {
			const botId = botIds[i];
			bots.current[botId].emit('start', teamIds.map(teamId => ({
				gameId, teamId,
				playersCount: i === botIds.length-1 ? playersCount - perBot * (botIds.length-1) : perBot,
			})));
		}
	}
	const stopTest = () => {
		for(let i in bots.current) {
			bots.current[i].emit('stop');
		}
		dispatch({event: 'game_end'});
	}
	const clear = () => {
		dispatch({event: 'game_clear'})
	}
	return (
		<>
			<div className={`header ${!data.bots && !data.running?'nostats':''}`}>
				<BasicStatus sockets={data.sockets} bots={data.bots} joinApis={data.joinApis} />
				<div className="flex" />
				{data.running?(
					<span className="btn" onClick={() => stopTest()}>
						{"Stop "}
						<StopIcon className="icon" />
					</span>
				):data.bots?(
					<span className="btn" onClick={() => clear()}>
						{"Clear "}
						<CloseIcon className="icon" />
					</span>
				):(
					<>
						<span onClick={() => setAddModal(true)} className="btn">
							{"Add "}
							<AddIcon className="icon" />
						</span>
						<span onClick={() => setStartModal(true)} className="btn">
							{"Start "}
							<PlayIcon className="icon" />
						</span>
						{/* <button className="" onClick={() => setAddModal(true)}>Add</button>
						<button className="" onClick={() => setStartModal(true)}>Start</button> */}
					</>
				)}
			</div>
			<div className={`body ${!data.bots && !data.running?'nostats':''}`}>
				{Object.keys(data.processes).length?(
					<>
						<div className="title">Processes</div>
						<div className="">
							{Object.keys(data.processes).map(i => (
								<ProcessStatus {...data.processes[i]} retry={() => connectProcess(data.processes[i].url, i)} canRetry={!data.processes[i].connected && !data.running} key={i} />
							))}
						</div>
					</>
				):null}
				{Object.keys(data.questions).length?(
					<>
						<div className="title">Questions</div>
						<div className="">
							{Object.keys(data.questions).map(i => (
								<QuestionStatus {...data.questions[i]} key={i} />
							))}
						</div>
					</>
				):null}
			</div>
			<SetModal
				visible={startModal}
				onClose={() => setStartModal(false)}
				onStart={(gid, tids, playersCount) => startTest(gid, tids, playersCount)}
			/>
			<AddModal
				visible={addModal}
				onClose={() => setAddModal(false)}
				onAdd={connectProcess}
			/>
		</>
	)
}

function BasicStatus ({sockets, joinApis, bots}: {sockets: number, bots: number, joinApis: joinApisType}) {
	return (
		<>
			<div className="">
				<span className=''>BOTS </span>
				<Stats className=''>{bots}</Stats>
			</div>
			<div className="">
				<span className=''>JOIN </span>
				<Stats className='text-blue'>{joinApis.pending}</Stats>
				<Separator />
				<Stats className='text-green'>{joinApis.success}</Stats>
				<Separator />
				<Stats className='text-red'>{joinApis.failed}</Stats>
			</div>
			<div className="">
				<span className=''>ONLINE </span>
				<Stats className=''>{sockets}</Stats>
			</div>
		</>
	)
}
function ProcessStatus (data: processType & {retry: () =>  void, canRetry: boolean}) {
	return (
		<div className={`process ${data.connected?'connected':'disconnected'}`}>
			<div className="process-header">
				<div className="process-url">{data.url}</div>
				<Separator />
				<div className={`process-connection ${data.connected?'text-green':'text-red'}`}>
					{data.connected?'Online':'Offline'}
				</div>
				{data.canRetry && (
					<>
						<Separator />
						<span className="btn" onClick={data.retry}>
							{"Retry "}
							<RefreshIcon className="icon" />
						</span>
					</>
				)}
			</div>
			<div className="process-body">
				<BasicStatus sockets={data.sockets} bots={data.bots} joinApis={data.joinApis} />
			</div>
		</div>
	)
}
function QuestionStatus (data: questionType) {
	return (
		<div className="question">
			<div className="">
				{"Received: "}
				<Stats className="text-white">{data.asked}</Stats>
			</div>
			<div className="">
				{"Answered: "}
				<Stats className="text-white">{data.pending}</Stats>
				<Separator />
				<Stats className="text-green">{Object.values(data.answered).reduce((a,i) => a+i, 0)}</Stats>
				<Separator />
				<Stats className="text-red">{data.failed}</Stats>
			</div>
			<div className="">
				{"Answers: "}
				{Object.keys(data.answered).map((i, index, array) => (
					<Fragment key={i}>
						<Stats className="">{data.answered[i]}</Stats>
						{index!==array.length-1?<Separator />:null}
					</Fragment>
				))}
			</div>
		</div>
	)
}

function AddModal ({ visible, onClose, onAdd }: {
	visible: boolean,
	onClose: () => void,
	onAdd: (url: string) => void,
}) {
	const [url, setUrl] = useState<string>('');
	return visible ? (
		<div className="modal-outer">
			<div className="modal-bg" />
			<div className="modal-inner">
				<div className="modal-header">
					<div className="modal-title">Connect a Process</div>
					<div className="modal-close btn" onClick={onClose}>
						<CloseIcon className="icon" />
					</div>
				</div>
				<div className="modal-body">
					<div className="modal-field">
						Process URL:
						<input onChange={e => setUrl(e.target.value)} className="flex" />
					</div>
				</div>
				<div className="modal-footer">
					<button onClick={() => onClose()} className="modal-button">Cancel</button>
					<button
						disabled={!url}
						onClick={() => {if(url.length) {onClose();onAdd(url)}}}
						className="modal-button"
					>Connect Process</button>
				</div>
			</div>
		</div>
	) : null;
}

function SetModal ({ visible, onClose, onStart }: {
	visible: boolean,
	onClose: () => void,
	onStart: (gameId: number, teamIds: number[], playersCount: number) => void,
}) {
	const [gameId, setGameId] = useState<number | null>(null);
	const [playersCount, setPlayersCount] = useState<number | null>(null);
	const [teamIds, setTeamIds] = useState<number[]>([]);
	return visible ? (
		<div className="modal-outer">
			<div className="modal-bg" />
			<div className="modal-inner">
				<div className="modal-header">
					<div className="modal-title">Join a Game</div>
					<div className="modal-close btn" onClick={onClose}>
						<CloseIcon className="icon" />
					</div>
				</div>
				<div className="modal-body">
					<div className="modal-field">
						Game ID:
						<div className="flex" />
						<NumberInputDisplay onChange={v => setGameId(v)} value={gameId || 0} />
					</div>
					<div className="modal-field">
					Team IDs:
						<div className="flex" />
					<ArrayInput
						array={teamIds}
						onChange={setTeamIds}
						arrayItem={({onClose: onItemClose, value}) => (
							<div className="array-item-teamid">
								{value}
								<span onClick={onItemClose} className="btn">
									<CloseIcon className="icon" />
								</span>
							</div>
						)}
						inputItem={({onChange, onCancel}) => <NumberInput onChange={v => v ? onChange(v) : onCancel()} value={0} />}
					/>
					</div>
					<div className="modal-field">
					Players per Team:
						<div className="flex" />
					<NumberInputDisplay max={100} onChange={v => setPlayersCount(v)} value={playersCount || 0} />
					</div>
				</div>
				<div className="modal-footer">
					<button onClick={() => onClose()} className="modal-button">Cancel</button>
					<button
						disabled={!gameId || !teamIds.length}
						onClick={() => {if(gameId && playersCount && teamIds.length) {onClose();onStart(gameId, teamIds, playersCount)}}}
						className="modal-button"
					>Start Game</button>
				</div>
			</div>
		</div>
	) : null;
}

function NumberInput ({ onChange, value, max }: {onChange: (v: number) => void, value: number, max?: number}) {
	const [num, setNum] = useState(value+'');
	const disabled = useMemo<boolean>(() => {
		const val = Number(num);
		if(isNaN(val)) return true;
		if(typeof max === 'number' && val>max)
			return true;
		return false;
	}, [num, max]);
	const onClick = () => {
		if(!disabled) {
			onChange(Number(num));
			setNum('');
			return true;
		}
		return false;
	}
	const onExit = () => {
		onChange(value);
	}
	const onKeyDown: KeyboardEventHandler<HTMLInputElement> = (e) => {
		if(e.key==="Enter")
			onClick();
		if(e.key==="Escape")
			onExit();
	}
	return (
		<>
			<input onBlur={() => onClick() || onExit()} onKeyDown={onKeyDown} autoFocus type="number" value={num} onChange={e => setNum(e.target.value)} />
			<span onClick={onClick} className="btn">
				<TickIcon className="icon" />
			</span>
		</>
	)
}

function NumberInputDisplay ({ onChange, value, max }: { onChange: (v: number) => void, value: number, max?: number }) {
	const [edit, setEdit] = useState(false);
	return (
		<>
			{edit ? <NumberInput max={max} onChange={v => {onChange(v); setEdit(false)}} value={value} /> : <>
				<span className="btn" onClick={() => setEdit(true)}>
					<span className="">{value}</span>
					<EditIcon className="icon" />
				</span>
			</>}
		</>
	)
}

function Separator () {
	return (
		<span className="separator">|</span>
	)
}

function Stats ({children, className}: {children: ReactNode, className: string}) {
	return (
		<>
			<span className={`stats ${className}`}>{children}</span>
			<span className={`nonstats ${className}`}>_</span>
		</>
	)
}